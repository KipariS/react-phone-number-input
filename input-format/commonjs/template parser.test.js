'use strict';

var _chai = require('chai');

var _templateParser = require('../source/template parser');

var _templateParser2 = _interopRequireDefault(_templateParser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('template parser', function () {
	it('should parse a phone number', function () {
		var parse = (0, _templateParser2.default)('x (xxx) xxx-xx-xx', function (character, value) {
			if (character >= '0' && character <= '9') {
				return character;
			}
		});

		(0, _chai.expect)(parse('', '')).to.be.undefined;
		(0, _chai.expect)(parse('a', '')).to.be.undefined;
		(0, _chai.expect)(parse('5', '8800555353')).to.equal('5');
		(0, _chai.expect)(parse('5', '88005553535')).to.be.undefined;
	});

	it('should accept placeholder parameter', function () {
		var parse = (0, _templateParser2.default)('A (AAA) AAA-AA-AA', 'A', function (character, value) {
			if (character >= '0' && character <= '9') {
				return character;
			}
		});

		(0, _chai.expect)(parse('5', '8800555353')).to.equal('5');
		(0, _chai.expect)(parse('5', '88005553535')).to.be.undefined;
	});
});
//# sourceMappingURL=template parser.test.js.map