'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _parse2 = require('./parse');

var _parse3 = _interopRequireDefault(_parse2);

var _inputControl = require('./input control');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Usage:
//
// <ReactInput
// 	value={this.state.phone}
// 	onChange={phone => this.setState({ phone })}
// 	parse={character => character}
// 	format={value => ({ text: value, template: 'xxxxxxxx' })}/>
//
var ReactInput = (_temp2 = _class = function (_React$Component) {
	_inherits(ReactInput, _React$Component);

	function ReactInput() {
		var _ref;

		var _temp, _this, _ret;

		_classCallCheck(this, ReactInput);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ReactInput.__proto__ || Object.getPrototypeOf(ReactInput)).call.apply(_ref, [this].concat(args))), _this), _this.storeInstance = function (instance) {
			_this.input = instance;
		}, _this.getInputElement = function () {
			return _this.input;
		}, _this.onChange = function (event) {
			var _this$props = _this.props,
			    parse = _this$props.parse,
			    format = _this$props.format;


			return (0, _inputControl.onChange)(event, _this.getInputElement(), parse, format, _this.props.onChange);
		}, _this.onPaste = function (event) {
			var _this$props2 = _this.props,
			    parse = _this$props2.parse,
			    format = _this$props2.format;


			return (0, _inputControl.onPaste)(event, _this.getInputElement(), parse, format, _this.props.onChange);
		}, _this.onCut = function (event) {
			var _this$props3 = _this.props,
			    parse = _this$props3.parse,
			    format = _this$props3.format;


			return (0, _inputControl.onCut)(event, _this.getInputElement(), parse, format, _this.props.onChange);
		}, _this.onBlur = function (event) {
			var _this$props4 = _this.props,
			    parse = _this$props4.parse,
			    onBlur = _this$props4.onBlur;

			// This `onBlur` interceptor is a workaround for `redux-form`,
			// so that it gets the right (parsed, not the formatted one)
			// `event.target.value` in its `onBlur` handler.

			if (onBlur) {
				var _event = _extends({}, event, {
					target: _extends({}, event.target, {
						value: (0, _parse3.default)(_this.getInputElement().value, undefined, parse).value
					})

					// For `redux-form` event detection.
					// https://github.com/erikras/redux-form/blob/v5/src/events/isEvent.js
				});_event.stopPropagation = event.stopPropagation;
				_event.preventDefault = event.preventDefault;

				onBlur(_event);
			}
		}, _this.onKeyDown = function (event) {
			var _this$props5 = _this.props,
			    parse = _this$props5.parse,
			    format = _this$props5.format;


			if (_this.props.onKeyDown) {
				_this.props.onKeyDown(event);
			}

			return (0, _inputControl.onKeyDown)(event, _this.getInputElement(), parse, format, _this.props.onChange);
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(ReactInput, [{
		key: 'render',
		value: function render() {
			var _props = this.props,
			    value = _props.value,
			    parse = _props.parse,
			    format = _props.format,
			    inputComponent = _props.inputComponent,
			    rest = _objectWithoutProperties(_props, ['value', 'parse', 'format', 'inputComponent']);

			// Non-string `inputComponent`s would work in this case
			// but it would also introduce a caret reset bug:
			// the caret position would reset on each input.
			// The origins of this bug are unknown, they may be
			// somehow related to the `ref` property
			// being intercepted by React here.


			return _react2.default.createElement(inputComponent, _extends({}, rest, {
				ref: this.storeInstance,
				value: format(isEmpty(value) ? '' : value).text,
				onKeyDown: this.onKeyDown,
				onChange: this.onChange,
				onPaste: this.onPaste,
				onCut: this.onCut,
				onBlur: this.onBlur
			}));
		}

		/**
   * Returns `<input/>` DOM Element.
   * @return {DOMElement}
   */


		// This handler is a workaround for `redux-form`.

	}, {
		key: 'focus',


		/**
   * Focuses the `<input/>`.
   * Can be called manually.
   */
		value: function focus() {
			this.getInputElement().focus();
		}
	}]);

	return ReactInput;
}(_react2.default.Component), _class.propTypes = {
	// Parses a single characher of `<input/>` text.
	parse: _propTypes2.default.func.isRequired,

	// Formats `value` into `<input/>` text.
	format: _propTypes2.default.func.isRequired,

	// Renders `<input/>` by default.
	// For some reason non-default `inputComponent`
	// will reset caret position.
	inputComponent: _propTypes2.default.string.isRequired,

	// `<input/>` `type` attribute.
	type: _propTypes2.default.string.isRequired,

	// Is parsed from <input/> text.
	value: _propTypes2.default.string,

	// This handler is called each time `<input/>` text is changed.
	onChange: _propTypes2.default.func.isRequired,

	// This `onBlur` interceptor is a workaround for `redux-form`,
	// so that it gets the parsed `value` in its `onBlur` handler,
	// not the formatted text.
	onBlur: _propTypes2.default.func,

	// Passthrough
	onKeyDown: _propTypes2.default.func
}, _class.defaultProps = {
	// Renders `<input/>` by default.
	// For some reason non-default `inputComponent`
	// will reset caret position.
	inputComponent: 'input',

	// `<input/>` `type` attribute.
	type: 'text'
}, _temp2);
exports.default = ReactInput;


function isEmpty(value) {
	return value === undefined || value === null;
}
//# sourceMappingURL=ReactInput.js.map