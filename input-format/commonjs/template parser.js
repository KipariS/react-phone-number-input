'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function (template, placeholder, parse) {
	if (typeof placeholder === 'function') {
		parse = placeholder;
		placeholder = 'x';
	}

	var max_characters = (0, _helpers.count_occurences)(placeholder, template);

	return function (character, value) {
		if (value.length < max_characters) {
			return parse(character, value);
		}
	};
};

var _helpers = require('./helpers');
//# sourceMappingURL=template parser.js.map