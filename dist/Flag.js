"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Default country flag icon.
// `<img/>` is wrapped in a `<div/>` to prevent SVGs from exploding in size in IE 11.
// https://github.com/catamphetamine/react-phone-number-input/issues/111
var FlagComponent = function FlagComponent(_ref) {
  var country = _ref.country,
      flags = _ref.flags,
      flagsPath = _ref.flagsPath;

  if (flags && flags[country]) {
    return flags[country]();
  }

  return /*#__PURE__*/_react["default"].createElement("img", {
    alt: country,
    className: "react-phone-number-input__icon-image",
    src: "".concat(flagsPath).concat(country.toLowerCase(), ".svg")
  });
};

FlagComponent.propTypes = {
  // The country to be selected by default.
  // Two-letter country code ("ISO 3166-1 alpha-2").
  country: _propTypes["default"].string.isRequired,
  // Country flag icon components.
  // By default flag icons are inserted as `<img/>`s
  // with their `src` pointed to `flag-icon-css` github repo.
  // There might be cases (e.g. an offline application)
  // where having a large (3 megabyte) `<svg/>` flags
  // bundle is more appropriate.
  // `import flags from 'react-phone-number-input/flags'`.
  flags: _propTypes["default"].objectOf(_propTypes["default"].func),
  // A base URL path for national flag SVG icons.
  // By default it uses the ones from `flag-icon-css` github repo.
  flagsPath: _propTypes["default"].string.isRequired
};
var _default = FlagComponent;
exports["default"] = _default;