"use strict";

var _inputControl = require("./input-control");

var _metadataMin = _interopRequireDefault(require("libphonenumber-js/metadata.min.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

describe('input-control', function () {
  it('should get pre-selected country', function () {
    // Can't return "International". Return the first country available.
    (0, _inputControl.getPreSelectedCountry)({}, null, ['US', 'RU'], false, _metadataMin["default"]).should.equal('US'); // Can return "International".
    // Country can't be derived from the phone number.

    expect((0, _inputControl.getPreSelectedCountry)({}, undefined, ['US', 'RU'], true, _metadataMin["default"])).to.be.undefined; // Derive country from the phone number.

    (0, _inputControl.getPreSelectedCountry)({
      country: 'RU',
      phone: '8005553535'
    }, null, ['US', 'RU'], false, _metadataMin["default"]).should.equal('RU'); // Country derived from the phone number overrides the supplied one.

    (0, _inputControl.getPreSelectedCountry)({
      country: 'RU',
      phone: '8005553535'
    }, 'US', ['US', 'RU'], false, _metadataMin["default"]).should.equal('RU'); // Only pre-select a country if it's in the available `countries` list.

    (0, _inputControl.getPreSelectedCountry)({
      country: 'RU',
      phone: '8005553535'
    }, null, ['US', 'DE'], false, _metadataMin["default"]).should.equal('US');
    expect((0, _inputControl.getPreSelectedCountry)({
      country: 'RU',
      phone: '8005553535'
    }, 'US', ['US', 'DE'], true, _metadataMin["default"])).to.be.undefined;
  });
  it('should generate country select options', function () {
    var defaultLabels = {
      'RU': 'Russia (Россия)',
      'US': 'United States',
      'ZZ': 'International'
    }; // Without custom country names.

    (0, _inputControl.getCountrySelectOptions)(['US', 'RU'], defaultLabels, false).should.deep.equal([{
      value: 'RU',
      label: 'Russia (Россия)'
    }, {
      value: 'US',
      label: 'United States'
    }]); // With custom country names.

    (0, _inputControl.getCountrySelectOptions)(['US', 'RU'], _objectSpread(_objectSpread({}, defaultLabels), {}, {
      'RU': 'Russia'
    }), false).should.deep.equal([{
      value: 'RU',
      label: 'Russia'
    }, {
      value: 'US',
      label: 'United States'
    }]); // With "International" (without custom country names).

    (0, _inputControl.getCountrySelectOptions)(['US', 'RU'], defaultLabels, true).should.deep.equal([{
      label: 'International'
    }, {
      value: 'RU',
      label: 'Russia (Россия)'
    }, {
      value: 'US',
      label: 'United States'
    }]); // With "International" (with custom country names).

    (0, _inputControl.getCountrySelectOptions)(['US', 'RU'], _objectSpread(_objectSpread({}, defaultLabels), {}, {
      'RU': 'Russia',
      ZZ: 'Intl'
    }), true).should.deep.equal([{
      label: 'Intl'
    }, {
      value: 'RU',
      label: 'Russia'
    }, {
      value: 'US',
      label: 'United States'
    }]);
  });
  it('should parse phone numbers', function () {
    (0, _inputControl.parsePhoneNumber)('+78005553535', _metadataMin["default"]).should.deep.equal({
      country: 'RU',
      phone: '8005553535'
    }); // No `value` passed.

    (0, _inputControl.parsePhoneNumber)(null, _metadataMin["default"]).should.deep.equal({});
  });
  it('should generate national number digits', function () {
    (0, _inputControl.generateNationalNumberDigits)({
      country: 'FR',
      phone: '509758351'
    }, _metadataMin["default"]).should.equal('0509758351');
  });
  it('should migrate parsed input for new country', function () {
    // No input. Returns `undefined`.
    (0, _inputControl.migrateParsedInputForNewCountry)('', 'RU', 'US', _metadataMin["default"]).should.equal(''); // Switching from "International" to a country
    // to which the phone number already belongs to.
    // No changes. Returns `undefined`.

    (0, _inputControl.migrateParsedInputForNewCountry)('+18005553535', null, 'US', _metadataMin["default"]).should.equal('+18005553535'); // Switching between countries. National number. No changes.

    (0, _inputControl.migrateParsedInputForNewCountry)('8005553535', 'RU', 'US', _metadataMin["default"]).should.equal('8005553535'); // Switching from "International" to a country.

    (0, _inputControl.migrateParsedInputForNewCountry)('+78005553535', null, 'US', _metadataMin["default"]).should.equal('+18005553535'); // Switching countries. International number.

    (0, _inputControl.migrateParsedInputForNewCountry)('+78005553535', 'RU', 'US', _metadataMin["default"]).should.equal('+18005553535'); // Switching countries. International number.
    // Country calling code is longer than the amount of digits available.

    (0, _inputControl.migrateParsedInputForNewCountry)('+99', 'KG', 'US', _metadataMin["default"]).should.equal('+1'); // Switching countries. International number. No such country code.

    (0, _inputControl.migrateParsedInputForNewCountry)('+99', 'KG', 'US', _metadataMin["default"]).should.equal('+1'); // Switching to "International". National number.

    (0, _inputControl.migrateParsedInputForNewCountry)('8800555', 'RU', null, _metadataMin["default"]).should.equal('+7800555'); // Switching to "International". International number. No changes.

    (0, _inputControl.migrateParsedInputForNewCountry)('+78005553535', 'RU', null, _metadataMin["default"]).should.equal('+78005553535'); // Prefer national format. Country matches.

    (0, _inputControl.migrateParsedInputForNewCountry)('+78005553535', null, 'RU', _metadataMin["default"], true).should.equal('8005553535'); // Prefer national format. Country doesn't match.

    (0, _inputControl.migrateParsedInputForNewCountry)('+78005553535', null, 'US', _metadataMin["default"], true).should.equal('+18005553535');
  });
  it('should format phone number in e164', function () {
    // No number.
    expect((0, _inputControl.e164)()).to.be.undefined; // International number. Just a '+' sign.

    expect((0, _inputControl.e164)('+')).to.be.undefined; // International number.

    (0, _inputControl.e164)('+7800').should.equal('+7800'); // National number. Without country.

    expect((0, _inputControl.e164)('8800', null)).to.be.undefined; // National number. With country. Just national prefix.

    expect((0, _inputControl.e164)('8', 'RU', _metadataMin["default"])).to.be.undefined; // National number. With country. Just national prefix.

    (0, _inputControl.e164)('8800', 'RU', _metadataMin["default"]).should.equal('+7800');
  });
  it('should trim the phone number if it exceeds the maximum length', function () {
    // No number.
    expect((0, _inputControl.trimNumber)()).to.be.undefined; // International number. Without country.

    (0, _inputControl.trimNumber)('+780055535351').should.equal('+780055535351'); // National number. Without country.

    (0, _inputControl.trimNumber)('880055535351', null).should.equal('880055535351'); // National number. Doesn't exceed the maximum length.

    (0, _inputControl.trimNumber)('88005553535', 'RU', _metadataMin["default"]).should.equal('88005553535'); // National number. Exceeds the maximum length.

    (0, _inputControl.trimNumber)('880055535351', 'RU', _metadataMin["default"]).should.equal('88005553535'); // International number. Doesn't exceed the maximum length.

    (0, _inputControl.trimNumber)('+78005553535', 'RU', _metadataMin["default"]).should.equal('+78005553535'); // International number. Exceeds the maximum length.

    (0, _inputControl.trimNumber)('+780055535351', 'RU', _metadataMin["default"]).should.equal('+78005553535');
  });
  it('should get country for parsed input', function () {
    // Just a '+' sign.
    (0, _inputControl.getCountryForParsedInput)('+', 'RU', ['US', 'RU'], true, _metadataMin["default"]).should.equal('RU');
    expect((0, _inputControl.getCountryForParsedInput)('+', undefined, ['US', 'RU'], true, _metadataMin["default"])).to.be.undefined; // A country can be derived.

    (0, _inputControl.getCountryForParsedInput)('+78005553535', undefined, ['US', 'RU'], true, _metadataMin["default"]).should.equal('RU'); // A country can't be derived yet.
    // And the currently selected country doesn't fit the number.

    expect((0, _inputControl.getCountryForParsedInput)('+7', 'FR', ['FR', 'RU'], true, _metadataMin["default"])).to.be.undefined;
    expect((0, _inputControl.getCountryForParsedInput)('+12', 'FR', ['FR', 'US'], true, _metadataMin["default"])).to.be.undefined; // A country can't be derived yet.
    // And the currently selected country doesn't fit the number.
    // Bit "International" option is not available.

    (0, _inputControl.getCountryForParsedInput)('+7', 'FR', ['FR', 'RU'], false, _metadataMin["default"]).should.equal('FR');
    (0, _inputControl.getCountryForParsedInput)('+12', 'FR', ['FR', 'US'], false, _metadataMin["default"]).should.equal('FR');
  });
  it('should get country from possibly incomplete international phone number', function () {
    // `001` country calling code.
    expect((0, _inputControl.get_country_from_possibly_incomplete_international_phone_number)('+800', _metadataMin["default"])).to.be.undefined; // Country can be derived.

    (0, _inputControl.get_country_from_possibly_incomplete_international_phone_number)('+33', _metadataMin["default"]).should.equal('FR'); // Country can't be derived yet.

    expect((0, _inputControl.get_country_from_possibly_incomplete_international_phone_number)('+12', _metadataMin["default"])).to.be.undefined;
  });
  it('should compare strings', function () {
    (0, _inputControl.compare_strings)('aa', 'ab').should.equal(-1);
    (0, _inputControl.compare_strings)('aa', 'aa').should.equal(0);
    (0, _inputControl.compare_strings)('aac', 'aab').should.equal(1);
  });
  it('should strip country calling code from a number', function () {
    // Number is longer than country calling code prefix.
    (0, _inputControl.strip_country_calling_code)('+7800', 'RU', _metadataMin["default"]).should.equal('800'); // Number is shorter than (or equal to) country calling code prefix.

    (0, _inputControl.strip_country_calling_code)('+3', 'FR', _metadataMin["default"]).should.equal('');
    (0, _inputControl.strip_country_calling_code)('+7', 'FR', _metadataMin["default"]).should.equal(''); // `country` doesn't fit the actual `number`.
    // Iterates through all available country calling codes.

    (0, _inputControl.strip_country_calling_code)('+7800', 'FR', _metadataMin["default"]).should.equal('800'); // No `country`.
    // And the calling code doesn't belong to any country.

    (0, _inputControl.strip_country_calling_code)('+999', null, _metadataMin["default"]).should.equal('');
  });
  it('should get national significant number part', function () {
    // International number.
    (0, _inputControl.get_national_significant_number_part)('+7800555', null, _metadataMin["default"]).should.equal('800555'); // National number.

    (0, _inputControl.get_national_significant_number_part)('8800555', 'RU', _metadataMin["default"]).should.equal('800555');
  });
  it('should determine of a number could belong to a country', function () {
    // Matching.
    (0, _inputControl.could_number_belong_to_country)('+7800', 'RU', _metadataMin["default"]).should.equal(true); // First digit already not matching.

    (0, _inputControl.could_number_belong_to_country)('+7800', 'FR', _metadataMin["default"]).should.equal(false); // First digit matching, second - not matching.

    (0, _inputControl.could_number_belong_to_country)('+33', 'AM', _metadataMin["default"]).should.equal(false); // Number is shorter than country calling code.

    (0, _inputControl.could_number_belong_to_country)('+99', 'KG', _metadataMin["default"]).should.equal(true);
  });
});