"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _custom = require("libphonenumber-js/custom");

var _reactLifecyclesCompat = require("react-lifecycles-compat");

var _excluded = ["onChange", "onFocus", "country", "metadata"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } Object.defineProperty(subClass, "prototype", { value: Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }), writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// `PureComponent` is only available in React >= 15.3.0.
var PureComponent = _react["default"].PureComponent || _react["default"].Component;
/**
 * `InputBasic`'s caret is not as "smart" as the default `inputComponent`'s
 * but still works good enough. When erasing or inserting digits in the middle
 * of a phone number the caret usually jumps to the end: this is the expected
 * behaviour and it's the workaround for the [Samsung Galaxy smart caret positioning bug](https://github.com/catamphetamine/react-phone-number-input/issues/75).
 */

/**
 * @class InputBasic
 */

var InputBasic = /*#__PURE__*/function (_PureComponent) {
  _inherits(InputBasic, _PureComponent);

  var _super = _createSuper(InputBasic);

  function InputBasic() {
    var _this;

    _classCallCheck(this, InputBasic);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {});

    _defineProperty(_assertThisInitialized(_this), "onChange", function (event) {
      var onChange = _this.props.onChange;
      var value = _this.state.value;
      var newValue = (0, _custom.parseIncompletePhoneNumber)(event.target.value); // By default, if a value is something like `"(123)"`
      // then Backspace would only erase the rightmost brace
      // becoming something like `"(123"`
      // which would give the same `"123"` value
      // which would then be formatted back to `"(123)"`
      // and so a user wouldn't be able to erase the phone number.
      // Working around this issue with this simple hack.

      if (newValue === value) {
        if (_this.format(newValue).indexOf(event.target.value) === 0) {
          // Trim the last digit (or plus sign).
          newValue = newValue.slice(0, -1);
        }
      } // Prevents React from resetting the `<input/>` caret position.
      // https://github.com/reactjs/react-redux/issues/525#issuecomment-254852039
      // https://github.com/facebook/react/issues/955


      _this.setState({
        value: newValue
      }, function () {
        return onChange(newValue);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onBlur", function (event) {
      var onBlur = _this.props.onBlur;
      var value = _this.state.value;

      if (onBlur) {
        // `event` is React's `SyntheticEvent`.
        // Its `.value` is read-only therefore cloning it.
        var _event = _objectSpread(_objectSpread({}, event), {}, {
          target: _objectSpread(_objectSpread({}, event.target), {}, {
            value: value
          })
        }); // Workaround for `redux-form` event detection.
        // https://github.com/erikras/redux-form/blob/v5/src/events/isEvent.js


        _event.stopPropagation = event.stopPropagation;
        _event.preventDefault = event.preventDefault;
        return onBlur(_event);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "focus", function () {
      return _this.input.focus();
    });

    _defineProperty(_assertThisInitialized(_this), "storeInput", function (ref) {
      return _this.input = ref;
    });

    return _this;
  }

  _createClass(InputBasic, [{
    key: "format",
    value: function format(value) {
      var _this$props = this.props,
          country = _this$props.country,
          metadata = _this$props.metadata;
      return (0, _custom.formatIncompletePhoneNumber)(value, country, metadata);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          onChange = _this$props2.onChange,
          onFocus = _this$props2.onFocus,
          country = _this$props2.country,
          metadata = _this$props2.metadata,
          rest = _objectWithoutProperties(_this$props2, _excluded); // Prevents React from resetting the `<input/>` caret position.
      // https://github.com/reactjs/react-redux/issues/525#issuecomment-254852039
      // https://github.com/facebook/react/issues/955


      var value = this.state.value;
      return /*#__PURE__*/_react["default"].createElement("input", _extends({
        type: "tel",
        autoComplete: "tel"
      }, rest, {
        ref: this.storeInput,
        value: this.format(value),
        onChange: this.onChange,
        onFocus: onFocus,
        onBlur: this.onBlur
      }));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: // Prevents React from resetting the `<input/>` caret position.
    // https://github.com/reactjs/react-redux/issues/525#issuecomment-254852039
    // https://github.com/facebook/react/issues/955
    function getDerivedStateFromProps(_ref) {
      var value = _ref.value;
      return {
        value: value
      };
    }
  }]);

  return InputBasic;
}(PureComponent);

exports["default"] = InputBasic;

_defineProperty(InputBasic, "propTypes", {
  // The parsed phone number.
  // E.g.: `""`, `"+"`, `"+123"`, `"123"`.
  value: _propTypes["default"].string.isRequired,
  // Updates the `value`.
  onChange: _propTypes["default"].func.isRequired,
  // Toggles the `--focus` CSS class.
  // https://github.com/catamphetamine/react-phone-number-input/issues/189
  onFocus: _propTypes["default"].func,
  // `onBlur` workaround for `redux-form`'s bug.
  onBlur: _propTypes["default"].func,
  // A two-letter country code for formatting `value`
  // as a national phone number (e.g. `(800) 555 35 35`).
  // E.g. "US", "RU", etc.
  // If no `country` is passed then `value`
  // is formatted as an international phone number.
  // (e.g. `+7 800 555 35 35`)
  country: _propTypes["default"].string,
  // `libphonenumber-js` metadata.
  metadata: _propTypes["default"].object.isRequired
});