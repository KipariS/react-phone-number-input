"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _Flag = _interopRequireDefault(require("./Flag"));

var _PropTypes = require("./PropTypes");

var _inputControl = require("./input-control");

var _custom2 = _interopRequireDefault(require("libphonenumber-js/custom"));

var _countries2 = require("./countries");

var _InputSmart = _interopRequireDefault(require("./InputSmart"));

var _excluded = ["name", "disabled", "autoComplete", "countrySelectTabIndex", "showCountrySelect", "style", "className", "inputClassName", "getInputClassName", "countrySelectProperties", "error", "indicateInvalid", "countrySelectComponent", "inputComponent", "ext", "countries", "countryOptions", "labels", "country", "flags", "flagComponent", "flagsPath", "international", "internationalIcon", "displayInitialValueAsLocalNumber", "onCountryChange", "limitMaxLength", "locale", "metadata"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } Object.defineProperty(subClass, "prototype", { value: Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }), writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// `PureComponent` is only available in React >= 15.3.0.
var PureComponent = _react["default"].PureComponent || _react["default"].Component;
/**
 *  @class PhoneNumberInput
 */

var PhoneNumberInput = /*#__PURE__*/function (_PureComponent) {
  _inherits(PhoneNumberInput, _PureComponent);

  var _super = _createSuper(PhoneNumberInput);

  function PhoneNumberInput(props) {
    var _this;

    _classCallCheck(this, PhoneNumberInput);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onCountryChange", function (new_country) {
      var _this$props = _this.props,
          metadata = _this$props.metadata,
          onChange = _this$props.onChange,
          displayInitialValueAsLocalNumber = _this$props.displayInitialValueAsLocalNumber;
      var _this$state = _this.state,
          old_parsed_input = _this$state.parsed_input,
          old_country = _this$state.country; // After the new `country` has been selected,
      // if the phone number `<input/>` holds any digits
      // then migrate those digits for the new `country`.

      var new_parsed_input = (0, _inputControl.migrateParsedInputForNewCountry)(old_parsed_input, old_country, new_country, metadata, displayInitialValueAsLocalNumber);
      var new_value = (0, _inputControl.e164)(new_parsed_input, new_country, metadata); // Focus phone number `<input/>` upon country selection.

      _this.focus(); // If the user has already manually selected a country
      // then don't override that already selected country
      // if the default `country` property changes.
      // That's what `hasUserSelectedACountry` flag is for.


      _this.setState({
        country: new_country,
        hasUserSelectedACountry: true,
        parsed_input: new_parsed_input,
        value: new_value
      }, function () {
        // Update the new `value` property.
        // Doing it after the `state` has been updated
        // because `onChange()` will trigger `getDerivedStateFromProps()`
        // with the new `value` which will be compared to `state.value` there.
        onChange(new_value);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onPhoneNumberKeyDown", function (event) {
      var onKeyDown = _this.props.onKeyDown; // Actually "Down arrow" key is used for showing "autocomplete" ("autofill") options.
      // (e.g. previously entered phone numbers for `autoComplete="tel"`)
      // so can't hijack "Down arrow" keypress here.
      // // Expand country `<select/>`` on "Down arrow" key press.
      // if (event.keyCode === 40) {
      // 	this.country_select.toggle()
      // }

      if (onKeyDown) {
        onKeyDown(event);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (parsed_input) {
      var _this$props2 = _this.props,
          onChange = _this$props2.onChange,
          countries = _this$props2.countries,
          international = _this$props2.international,
          limitMaxLength = _this$props2.limitMaxLength,
          metadata = _this$props2.metadata;
      var country = _this.state.country;
      var parsed_input2; // by Tolick

      if (country) {
        var _metadata = new _custom2["default"].Metadata(metadata);

        _metadata.country(country);

        var possibleLengths = _metadata.possibleLengths();

        var maxLength = possibleLengths[possibleLengths.length - 1];
        var nationalSignificantNumberPart = (0, _inputControl.get_national_significant_number_part)(parsed_input, country, metadata);

        if (nationalSignificantNumberPart.length == maxLength + 1) {
          parsed_input = _this.state.value;
        }
      }

      if (parsed_input) {
        // by Tolick
        parsed_input2 = parsed_input; // by Tolick

        if (parsed_input2.length <= 1) {
          country = undefined;
        } // by Tolick


        if (parsed_input2[0] !== '+' && parsed_input2.length > 0) {
          parsed_input2 = '+' + parsed_input2.substring(0);
        } // If the phone number being input is an international one
        // then tries to derive the country from the phone number.
        // (regardless of whether there's any country currently selected)


        if (parsed_input2[0] === '+') {
          country = (0, _inputControl.getCountryForParsedInput)(parsed_input2, country, countries, international, metadata);
        } // If this `onChange()` event was triggered
        // as a result of selecting "International" country
        // then force-prepend a `+` sign if the phone number
        // `<input/>` value isn't in international format.
        else if (!country) {
          parsed_input2 = '+' + parsed_input2;
          country = (0, _inputControl.getCountryForParsedInput)(parsed_input2, country, countries, international, metadata);
        } // by Tolick

      } else {
        country = undefined;
      } // by Tolick
      // Trim the input to not exceed the maximum possible number length.


      if (limitMaxLength) {
        parsed_input2 = (0, _inputControl.trimNumber)(parsed_input2, country, metadata);
      } // Generate the new `value` property.


      var value = (0, _inputControl.e164)(parsed_input2, country, metadata);

      _this.setState({
        parsed_input: parsed_input2,
        value: value,
        country: country
      }, // Update the new `value` property.
      // Doing it after the `state` has been updated
      // because `onChange()` will trigger `getDerivedStateFromProps()`
      // with the new `value` which will be compared to `state.value` there.
      function () {
        return onChange(value);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "_onFocus", function () {
      return _this.setState({
        isFocused: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "_onBlur", function () {
      return _this.setState({
        isFocused: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onFocus", function (event) {
      var onFocus = _this.props.onFocus;

      _this._onFocus();

      if (onFocus) {
        onFocus(event);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onBlur", function (event) {
      var onBlur = _this.props.onBlur;
      var value = _this.state.value;

      _this._onBlur();

      if (!onBlur) {
        return;
      } // `event` is React's `SyntheticEvent`.
      // Its `.value` is read-only therefore cloning it.


      var _event = _objectSpread(_objectSpread({}, event), {}, {
        target: _objectSpread(_objectSpread({}, event.target), {}, {
          value: value
        })
      }); // For `redux-form` event detection.
      // https://github.com/erikras/redux-form/blob/v5/src/events/isEvent.js


      _event.stopPropagation = event.stopPropagation;
      _event.preventDefault = event.preventDefault;
      return onBlur(_event);
    });

    _defineProperty(_assertThisInitialized(_this), "hidePhoneInputField", function (hide) {
      _this.setState({
        hidePhoneInputField: hide
      });
    });

    _defineProperty(_assertThisInitialized(_this), "focus", function () {
      return _this.number_input.focus();
    });

    _defineProperty(_assertThisInitialized(_this), "storeCountrySelectInstance", function (_) {
      return _this.country_select = _;
    });

    _defineProperty(_assertThisInitialized(_this), "storePhoneNumberInputInstance", function (_) {
      return _this.number_input = _;
    });

    var _this$props3 = _this.props,
        _value = _this$props3.value,
        _country = _this$props3.country,
        _countries = _this$props3.countries,
        countryOptions = _this$props3.countryOptions,
        labels = _this$props3.labels,
        _international = _this$props3.international,
        _metadata2 = _this$props3.metadata;

    if (_country) {
      validateCountry(_country, _metadata2);
    }

    if (_countries) {
      validateCountries(_countries, _metadata2);
    }

    if (countryOptions) {
      validateCountryOptions(countryOptions, _metadata2);
    }

    var parsed_number = (0, _inputControl.parsePhoneNumber)(_value, _metadata2);
    var pre_selected_country = (0, _inputControl.getPreSelectedCountry)(parsed_number, _country, _countries || (0, _countries2.getCountryCodes)(labels), _international, _metadata2);
    _this.state = {
      // Workaround for `this.props` inside `getDerivedStateFromProps()`.
      props: _this.props,
      // The country selected.
      country: pre_selected_country,
      // Generate country `<select/>` options.
      country_select_options: generate_country_select_options(_this.props),
      // `parsed_input` state property holds non-formatted user's input.
      // The reason is that there's no way of finding out
      // in which form should `value` be displayed: international or national.
      // E.g. if `value` is `+78005553535` then it could be input
      // by a user both as `8 (800) 555-35-35` and `+7 800 555 35 35`.
      // Hence storing just `value`is not sufficient for correct formatting.
      // E.g. if a user entered `8 (800) 555-35-35`
      // then value is `+78005553535` and `parsed_input` is `88005553535`
      // and if a user entered `+7 800 555 35 35`
      // then value is `+78005553535` and `parsed_input` is `+78005553535`.
      parsed_input: generateParsedInput(_value, parsed_number, _this.props),
      // `value` property is duplicated in state.
      // The reason is that `getDerivedStateFromProps()`
      // needs this `value` to compare to the new `value` property
      // to find out if `parsed_input` needs updating:
      // If the `value` property was changed externally
      // then it won't be equal to `state.value`
      // in which case `parsed_input` and `country` should be updated.
      value: _value
    };
    return _this;
  }

  _createClass(PhoneNumberInput, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props4 = this.props,
          country = _this$props4.country,
          onCountryChange = _this$props4.onCountryChange;
      var selectedCountry = this.state.country;

      if (onCountryChange && selectedCountry !== country) {
        onCountryChange(selectedCountry);
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this$props5 = this.props,
          country = _this$props5.country,
          countries = _this$props5.countries,
          countryOptions = _this$props5.countryOptions,
          onCountryChange = _this$props5.onCountryChange,
          metadata = _this$props5.metadata;

      if (country && country !== prevProps.country) {
        validateCountry(country, metadata);
      }

      if (countries && countries !== prevProps.countries) {
        validateCountries(countries, metadata);
      }

      if (countryOptions && countryOptions !== prevProps.countryOptions) {
        validateCountryOptions(countryOptions, metadata);
      }

      if (onCountryChange && this.state.country !== prevState.country) {
        onCountryChange(this.state.country);
      }
    } // Country `<select/>` `onChange` handler.

  }, {
    key: "render",
    value: function render() {
      var _this$props6 = this.props,
          name = _this$props6.name,
          disabled = _this$props6.disabled,
          autoComplete = _this$props6.autoComplete,
          countrySelectTabIndex = _this$props6.countrySelectTabIndex,
          showCountrySelect = _this$props6.showCountrySelect,
          style = _this$props6.style,
          className = _this$props6.className,
          inputClassName = _this$props6.inputClassName,
          getInputClassName = _this$props6.getInputClassName,
          countrySelectProperties = _this$props6.countrySelectProperties,
          error = _this$props6.error,
          indicateInvalid = _this$props6.indicateInvalid,
          CountrySelectComponent = _this$props6.countrySelectComponent,
          InputComponent = _this$props6.inputComponent,
          ext = _this$props6.ext,
          countries = _this$props6.countries,
          countryOptions = _this$props6.countryOptions,
          labels = _this$props6.labels,
          _ = _this$props6.country,
          flags = _this$props6.flags,
          flagComponent = _this$props6.flagComponent,
          flagsPath = _this$props6.flagsPath,
          international = _this$props6.international,
          internationalIcon = _this$props6.internationalIcon,
          displayInitialValueAsLocalNumber = _this$props6.displayInitialValueAsLocalNumber,
          onCountryChange = _this$props6.onCountryChange,
          limitMaxLength = _this$props6.limitMaxLength,
          locale = _this$props6.locale,
          metadata = _this$props6.metadata,
          phoneNumberInputProps = _objectWithoutProperties(_this$props6, _excluded);

      var _this$state2 = this.state,
          country = _this$state2.country,
          hidePhoneInputField = _this$state2.hidePhoneInputField,
          country_select_options = _this$state2.country_select_options,
          parsed_input = _this$state2.parsed_input,
          isFocused = _this$state2.isFocused; // const InputComponent = inputComponent || (smartCaret ? InputSmart : InputBasic)
      // Extract `countrySelectProperties` from `this.props`
      // also removing them from `phoneNumberInputProps`.

      var _countrySelectProps = {};

      if (countrySelectProperties) {
        for (var key in countrySelectProperties) {
          if (this.props.hasOwnProperty(key)) {
            _countrySelectProps[countrySelectProperties[key]] = this.props[key];
            delete phoneNumberInputProps[key];
          }
        }
      }

      return /*#__PURE__*/_react["default"].createElement("div", {
        style: style,
        className: (0, _classnames["default"])('react-phone-number-input', {
          'react-phone-number-input--focus': isFocused,
          'react-phone-number-input--invalid': error && indicateInvalid
        }, className)
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "react-phone-number-input__row"
      }, showCountrySelect && /*#__PURE__*/_react["default"].createElement(CountrySelectComponent, _extends({}, _countrySelectProps, {
        ref: this.storeCountrySelectInstance,
        name: name ? "".concat(name, "__country") : undefined,
        value: country,
        options: country_select_options,
        onChange: this.onCountryChange,
        onFocus: this._onFocus,
        onBlur: this._onBlur,
        disabled: disabled,
        tabIndex: countrySelectTabIndex,
        hidePhoneInputField: this.hidePhoneInputField,
        focusPhoneInputField: this.focus,
        className: "react-phone-number-input__country"
      })), !hidePhoneInputField && /*#__PURE__*/_react["default"].createElement(InputComponent, _extends({
        type: "tel",
        name: name ? name : 'phone'
      }, phoneNumberInputProps, {
        ref: this.storePhoneNumberInputInstance,
        metadata: metadata,
        country: country,
        value: parsed_input || '',
        onChange: this.onChange,
        onFocus: this.onFocus,
        onBlur: this.onBlur,
        onKeyDown: this.onPhoneNumberKeyDown,
        disabled: disabled,
        autoComplete: autoComplete,
        className: (0, _classnames["default"])('react-phone-number-input__input', 'react-phone-number-input__phone', {
          'react-phone-number-input__input--disabled': disabled,
          'react-phone-number-input__input--invalid': error && indicateInvalid
        }, inputClassName, getInputClassName && getInputClassName({
          disabled: disabled,
          invalid: error && indicateInvalid
        }))
      })), ext && !hidePhoneInputField && /*#__PURE__*/_react["default"].createElement("label", {
        className: "react-phone-number-input__ext"
      }, labels.ext, /*#__PURE__*/_react["default"].cloneElement(ext, {
        type: ext.props.type === undefined ? 'number' : ext.props.type,
        onFocus: this._onFocus,
        onBlur: this._onBlur,
        className: (0, _classnames["default"])('react-phone-number-input__input', 'react-phone-number-input__ext-input', {
          'react-phone-number-input__input--disabled': disabled
        }, inputClassName, getInputClassName && getInputClassName({
          disabled: disabled
        }), ext.props.className)
      }))), error && indicateInvalid && /*#__PURE__*/_react["default"].createElement("div", {
        className: "react-phone-number-input__error"
      }, error));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: // `state` holds previous props as `props`, and also:
    // * `country` — The currently selected country, e.g. `"RU"`.
    // * `value` — The currently entered phone number (E.164), e.g. `+78005553535`.
    // * `parsed_input` — The parsed `<input/>` value, e.g. `8005553535`.
    // (and a couple of other less significant properties)
    function getDerivedStateFromProps(props, state) {
      var country = state.country,
          hasUserSelectedACountry = state.hasUserSelectedACountry,
          value = state.value,
          _state$props = state.props,
          old_default_country = _state$props.country,
          old_value = _state$props.value;
      var metadata = props.metadata,
          new_default_country = props.country,
          new_value = props.value;
      var new_state = {
        // Emulate `prevProps` via `state.props`.
        props: props,
        // If the user has already manually selected a country
        // then don't override that already selected country
        // if the default `country` property changes.
        // That's what `hasUserSelectedACountry` flag is for.
        hasUserSelectedACountry: hasUserSelectedACountry
      }; // If `countries` or `labels` or `international` changed
      // then re-generate country `<select/>` options.

      if (props.countries !== state.props.countries || props.labels !== state.props.labels || props.international !== state.props.international) {
        new_state.country_select_options = generate_country_select_options(props);
      } // If the default country changed.
      // (e.g. in case of ajax GeoIP detection after page loaded)
      // then select it but only if the user hasn't already manually
      // selected a country and no phone number has been entered so far.
      // Because if the user has already started inputting a phone number
      // then he's okay with no country being selected at all ("International")
      // and doesn't want to be disturbed, doesn't want his input to be screwed, etc.


      if (new_default_country !== old_default_country && !hasUserSelectedACountry && !value && !new_value) {
        return _objectSpread(_objectSpread({}, new_state), {}, {
          country: new_default_country // `value` is `undefined`.
          // `parsed_input` is `undefined` because `value` is `undefined`.

        });
      } // If a new `value` is set externally.
      // (e.g. as a result of an ajax API request
      //  to get user's phone after page loaded)
      // The first part — `new_value !== old_value` —
      // is basically `props.value !== prevProps.value`
      // so it means "if value property was changed externally".
      // The second part — `new_value !== value` —
      // is for ignoring the `getDerivedStateFromProps()` call
      // which happens in `this.onChange()` right after `this.setState()`.
      // If this `getDerivedStateFromProps()` call isn't ignored
      // then the country flag would reset on each input.
      else if (new_value !== old_value && new_value !== value) {
        var parsed_number = (0, _inputControl.parsePhoneNumber)(new_value, metadata);
        return _objectSpread(_objectSpread({}, new_state), {}, {
          parsed_input: generateParsedInput(new_value, parsed_number, props),
          value: new_value,
          country: new_value ? parsed_number.country : country
        });
      } // `country` didn't change.
      // `value` didn't change.
      // `parsed_input` didn't change, because `value` didn't change.
      //
      // Maybe `new_state.country_select_options` changed.
      // In any case, update `prevProps`.


      return new_state;
    }
  }]);

  return PhoneNumberInput;
}(PureComponent); // Generates country `<select/>` options.


exports["default"] = PhoneNumberInput;

_defineProperty(PhoneNumberInput, "propTypes", {
  /**
   * Phone number in `E.164` format.
   *
   * Example:
   *
   * `"+12223333333"`
   */
  value: _propTypes["default"].string,

  /**
   * Updates the `value` as the user inputs the phone number.
   */
  onChange: _propTypes["default"].func.isRequired,

  /**
   * Toggles the `--focus` CSS class.
   * @ignore
   */
  onFocus: _propTypes["default"].func,

  /**
   * `onBlur` is usually passed by `redux-form`.
   * @ignore
   */
  onBlur: _propTypes["default"].func,

  /**
   * `onKeyDown` handler (e.g. to handle Enter key press).
   * @ignore
   */
  onKeyDown: _propTypes["default"].func,

  /**
   * Disables both the phone number `<input/>`
   * and the country `<select/>`.
   */
  // (is `false` by default)
  disabled: _propTypes["default"].bool.isRequired,

  /**
   * Sets `autoComplete` property for phone number `<input/>`.
   *
   * Web browser's "autocomplete" feature
   * remembers the phone number being input
   * and can also autofill the `<input/>`
   * with previously remembered phone numbers.
   *
   * https://developers.google.com/web/updates/2015/06/checkout-faster-with-autofill
   *
   * For example, can be used to turn it off:
   *
   * "So when should you use `autocomplete="off"`?
   *  One example is when you've implemented your own version
   *  of autocomplete for search. Another example is any form field
   *  where users will input and submit different kinds of information
   *  where it would not be useful to have the browser remember
   *  what was submitted previously".
   */
  // (is `"tel"` by default)
  autoComplete: _propTypes["default"].string.isRequired,

  /**
   * Set to `true` to show the initial `value` in
   * "national" format rather than "international".
   *
   * For example, if this flag is set to `true`
   * and the initial `value="+12133734253"` is passed
   * then the `<input/>` value will be `"(213) 373-4253"`.
   *
   * By default, this flag is set to `false`,
   * meaning that if the initial `value="+12133734253"` is passed
   * then the `<input/>` value will be `"+1 213 373 4253"`.
   *
   * The reason for such default behaviour is that
   * the newer generation grows up when there are no stationary phones
   * and therefore everyone inputs phone numbers in international format
   * in their smartphones so people gradually get more accustomed to
   * writing phone numbers in international format rather than in local format.
   * Future people won't be using "national" format, only "international".
   */
  // (is `false` by default)
  displayInitialValueAsLocalNumber: _propTypes["default"].bool.isRequired,

  /**
   * The country to be selected by default.
   * For example, can be set after a GeoIP lookup.
   *
   * Example: `"US"`.
   */
  // A two-letter country code ("ISO 3166-1 alpha-2").
  country: _propTypes["default"].string,

  /**
   * If specified, only these countries will be available for selection.
   *
   * Example:
   *
   * `["RU", "UA", "KZ"]`
   */
  countries: _propTypes["default"].arrayOf(_propTypes["default"].string),

  /**
   * Custom country `<select/>` option names.
   *
   * Example:
   *
   * `{ "ZZ": "Международный", RU: "Россия", US: "США", ... }`
   */
  labels: _PropTypes.labels.isRequired,

  /**
   * The base URL path for country flag icons.
   * By default it loads country flag icons from
   * `flag-icon-css` repo github pages website.
   * I imagine someone might want to download
   * those country flag icons and host them
   * on their own servers instead.
   */
  flagsPath: _propTypes["default"].string.isRequired,

  /**
   * Custom country flag icon components.
   * These flags replace the default ones.
   *
   * The shape is an object where keys are country codes
   * and values are flag icon components.
   * Flag icon components receive the same properties
   * as `flagComponent` (see below).
   *
   * Example:
   *
   * `{ "RU": () => <img src="..."/> }`
   *
   * Can be used to replace the default flags
   * with custom ones for certain (or all) countries.
   *
   * Can also be used to bundle `<svg/>` flags instead of `<img/>`s:
   *
   * By default flag icons are inserted as `<img/>`s
   * with their `src` pointed to `flag-icon-css` repo github pages website.
   *
   * There might be some cases
   * (e.g. a standalone "native" app, or an "intranet" web application)
   * when including the full set of `<svg/>` country flags (3 megabytes)
   * is more appropriate than downloading them individually at runtime only if needed.
   *
   * Example:
   *
   * `// Uses <svg/> flags (3 megabytes):`
   *
   * `import flags from 'react-phone-number-input/flags'`
   *
   * `import PhoneInput from 'react-phone-number-input'`
   *
   * `<PhoneInput flags={flags} .../>`
   */
  flags: _propTypes["default"].objectOf(_propTypes["default"].func),

  /**
   * Country flag icon component.
   *
   * Takes properties:
   *
   * * country : string — The country code.
   * * flagsPath : string — The `flagsPath` property (see above).
   * * flags : object — The `flags` property (see above).
   */
  flagComponent: _propTypes["default"].func.isRequired,

  /**
   * Set to `false` to drop the "International" option from country `<select/>`.
   */
  international: _propTypes["default"].bool.isRequired,

  /**
   * Custom "International" country `<select/>` option icon.
   */
  internationalIcon: _propTypes["default"].func.isRequired,

  /**
   * Set to `false` to hide country `<select/>`.
   */
  // (is `true` by default)
  showCountrySelect: _propTypes["default"].bool.isRequired,

  /**
   * HTML `tabindex` attribute for country `<select/>`.
   */
  countrySelectTabIndex: _propTypes["default"].number,

  /**
   * Can be used to place some countries on top of the list of country `<select/>` options.
   *
   * * `"|"` — inserts a separator.
   * * `"..."` — means "the rest of the countries" (can be omitted).
   *
   * Example:
   *
   * `["US", "CA", "AU", "|", "..."]`
   */
  countryOptions: _propTypes["default"].arrayOf(_propTypes["default"].string),

  /**
   * `<Phone/>` component CSS style object.
   */
  style: _propTypes["default"].object,

  /**
   * `<Phone/>` component CSS class.
   */
  className: _propTypes["default"].string,

  /**
   * Phone number `<input/>` CSS class.
   */
  inputClassName: _propTypes["default"].string,

  /**
   * Returns phone number `<input/>` CSS class string.
   * Receives an object of shape `{ disabled : boolean?, invalid : boolean? }`.
   * @ignore
   */
  getInputClassName: _propTypes["default"].func,

  /**
   * Country `<select/>` component.
   *
   * Receives properties:
   *
   * * `name : string?` — HTML `name` attribute.
   * * `value : string?` — The currently selected country code.
   * * `onChange(value : string?)` — Updates the `value`.
   * * `onFocus()` — Is used to toggle the `--focus` CSS class.
   * * `onBlur()` — Is used to toggle the `--focus` CSS class.
   * * `options : object[]` — The list of all selectable countries (including "International") each being an object of shape `{ value : string?, label : string, icon : React.Component }`.
   * * `disabled : boolean?` — HTML `disabled` attribute.
   * * `tabIndex : (number|string)?` — HTML `tabIndex` attribute.
   * * `className : string` — CSS class name.
   */
  //
  // (deprecated)
  // * `hidePhoneInputField(hide : boolean)` — Can be called to show/hide phone input field. Takes `hide : boolean` argument. E.g. `react-responsive-ui` `<Select/>` uses this to hide phone number input when country select is expanded.
  // * `focusPhoneInputField()` — Can be called to manually focus phone input field. E.g. `react-responsive-ui` `<Select/>` uses this to focus phone number input after country selection in a timeout (after the phone input field is no longer hidden).
  //
  countrySelectComponent: _propTypes["default"].func.isRequired,

  /**
   * Phone number `<input/>` component.
   *
   * Receives properties:
   *
   * * `value : string` — The parsed phone number. E.g.: `""`, `"+"`, `"+123"`, `"123"`.
   * * `onChange(value : string)` — Updates the `value`.
   * * `onFocus()` — Is used to toggle the `--focus` CSS class.
   * * `onBlur()` — Is used to toggle the `--focus` CSS class.
   * * `country : string?` — The currently selected country. `undefined` means "International" (no country selected).
   * * `metadata : object` — `libphonenumber-js` metadata.
   * * All other properties should be passed through to the underlying `<input/>`.
   *
   * Must also implement `.focus()` method.
   */
  inputComponent: _propTypes["default"].func.isRequired,

  /**
   * Set to `false` to use `inputComponent={InputBasic}`
   * instead of `input-format`'s `<ReactInput/>`.
   * Is `false` by default.
   */
  // smartCaret : PropTypes.bool.isRequired,

  /**
   * Phone number extension `<input/>` element.
   *
   * Example:
   *
   *	`ext={<input value={...} onChange={...}/>}`
   */
  ext: _propTypes["default"].node,

  /**
   * If set to `true` the phone number input will get trimmed
   * if it exceeds the maximum length for the country.
   */
  limitMaxLength: _propTypes["default"].bool.isRequired,

  /**
   * An error message to show below the phone number `<input/>`. For example, `"Required"`.
   */
  error: _propTypes["default"].string,

  /**
   * The `error` is shown only when `indicateInvalid` is `true`.
   * (which is the default).
   * @deprecated
   * @ignore
   */
  indicateInvalid: _propTypes["default"].bool,

  /**
   * Translation JSON object. See the `locales` directory for examples.
   */
  locale: _propTypes["default"].objectOf(_propTypes["default"].string),

  /**
   * `libphonenumber-js` metadata.
   *
   * Can be used to pass custom `libphonenumber-js` metadata
   * to reduce the overall bundle size for those who compile "custom" metadata.
   */
  metadata: _PropTypes.metadata.isRequired,

  /**
   * A long time ago a person requested an `onCountryChange(country)` event listener.
   * No valid reason was given other than compliance with some legacy code
   * which stored both phone number and country in a database.
   * @see  https://github.com/catamphetamine/react-phone-number-input/issues/128
   */
  onCountryChange: _propTypes["default"].func
});

_defineProperty(PhoneNumberInput, "defaultProps", {
  /**
   * Not disabled.
   */
  disabled: false,

  /**
   * Show `error` (if passed).
   * @deprecated
   */
  indicateInvalid: true,

  /**
   * Remember (and autofill) the value as a phone number.
   */
  autoComplete: 'tel',

  /**
   * Flag icon component.
   */
  flagComponent: _Flag["default"],

  /**
   * By default use icons from `flag-icon-css` github repo.
   */
  flagsPath: 'https://statics.travellizy.com/img/flags/',
  // flagsPath: 'https://lipis.github.io/flag-icon-css/flags/4x3/',

  /**
   * Default "International" country `<select/>` option icon (globe).
   */
  // internationalIcon: InternationalIcon,

  /**
   * Phone number `<input/>` component.
   */
  inputComponent: _InputSmart["default"],

  /**
   * Show country `<select/>`.
   */
  showCountrySelect: true,

  /**
   * Don't convert the initially passed phone number `value`
   * to a national phone number for its country.
   * The reason is that the newer generation grows up when
   * there are no stationary phones and therefore everyone inputs
   * phone numbers with a `+` in their smartphones
   * so phone numbers written in international form
   * are gradually being considered more natural than local ones.
   */
  displayInitialValueAsLocalNumber: false,

  /**
   * Set to `false` to use `inputComponent={InputBasic}`
   * instead of `input-format`'s `<ReactInput/>`.
   * Is `false` by default.
   */
  // smartCaret : false,

  /**
   * Whether to add the "International" option
   * to the list of countries.
   */
  international: true,

  /**
   * If set to `true` the phone number input will get trimmed
   * if it exceeds the maximum length for the country.
   */
  limitMaxLength: false
});

function generate_country_select_options(props) {
  var countries = props.countries,
      labels = props.labels,
      international = props.international,
      countryOptions = props.countryOptions;
  var CountrySelectOptionIcon = createCountrySelectOptionIconComponent(props);
  return transformCountryOptions((0, _inputControl.getCountrySelectOptions)(countries || (0, _countries2.getCountryCodes)(labels), labels, international).map(function (_ref) {
    var value = _ref.value,
        label = _ref.label;
    return {
      value: value,
      label: label,
      icon: CountrySelectOptionIcon
    };
  }), countryOptions);
}

function createCountrySelectOptionIconComponent(props) {
  var flags = props.flags,
      flagsPath = props.flagsPath,
      FlagComponent = props.flagComponent,
      InternationalIcon = props.internationalIcon;
  return function (_ref2) {
    var value = _ref2.value;
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: (0, _classnames["default"])('react-phone-number-input__icon', {
        'react-phone-number-input__icon--international': value === undefined
      })
    }, value ? /*#__PURE__*/_react["default"].createElement(FlagComponent, {
      country: value,
      flags: flags,
      flagsPath: flagsPath
    }) : /*#__PURE__*/_react["default"].createElement(InternationalIcon, null));
  };
} // Can move some country `<select/>` options
// to the top of the list, for example.
// See `countryOptions` property.


function transformCountryOptions(options, transform) {
  if (!transform) {
    return options;
  }

  var optionsOnTop = [];
  var optionsOnBottom = [];
  var appendTo = optionsOnTop;

  var _iterator = _createForOfIteratorHelper(transform),
      _step;

  try {
    var _loop = function _loop() {
      var element = _step.value;

      if (element === '|') {
        appendTo.push({
          divider: true
        });
      } else if (element === '...' || element === '…') {
        appendTo = optionsOnBottom;
      } else {
        // Find the position of the option.
        var index = options.indexOf(options.filter(function (option) {
          return option.value === element;
        })[0]); // Get the option.

        var option = options[index]; // Remove the option from its default position.

        options.splice(index, 1); // Add the option on top.

        appendTo.push(option);
      }
    };

    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      _loop();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return optionsOnTop.concat(options).concat(optionsOnBottom);
}

function generateParsedInput(value, parsed_number, props) {
  var displayInitialValueAsLocalNumber = props.displayInitialValueAsLocalNumber,
      metadata = props.metadata; // If the `value` (E.164 phone number)
  // belongs to the currently selected country
  // and `displayInitialValueAsLocalNumber` property is `true`
  // then convert `value` (E.164 phone number)
  // to a local phone number digits.
  // E.g. '+78005553535' -> '88005553535'.

  if (displayInitialValueAsLocalNumber && parsed_number.country) {
    return (0, _inputControl.generateNationalNumberDigits)(parsed_number, metadata);
  }

  return value;
}

function validateCountryOptions(countries, metadata) {
  var _iterator2 = _createForOfIteratorHelper(countries),
      _step2;

  try {
    for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
      var country = _step2.value;

      if (country && country !== '|' && country !== '...' && country !== '…') {
        if (!metadata.countries[country]) {
          throwCountryNotFound(country);
        }
      }
    }
  } catch (err) {
    _iterator2.e(err);
  } finally {
    _iterator2.f();
  }
}

function validateCountries(countries, metadata) {
  var _iterator3 = _createForOfIteratorHelper(countries),
      _step3;

  try {
    for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
      var country = _step3.value;

      if (!metadata.countries[country]) {
        throwCountryNotFound(country);
      }
    }
  } catch (err) {
    _iterator3.e(err);
  } finally {
    _iterator3.f();
  }
}

function validateCountry(country, metadata) {
  if (!metadata.countries[country]) {
    throwCountryNotFound(country);
  }
}

function throwCountryNotFound(country) {
  throw new Error("Country not found: ".concat(country));
}