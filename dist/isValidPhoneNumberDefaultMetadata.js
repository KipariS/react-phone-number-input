"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = isValidPhoneNumberDefaultMetadata;

var _custom = require("libphonenumber-js/custom");

var _metadataMin = _interopRequireDefault(require("libphonenumber-js/metadata.min.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function isValidPhoneNumberDefaultMetadata() {
  var parameters = Array.prototype.slice.call(arguments);
  parameters.push(_metadataMin["default"]);
  return _custom.isValidNumber.apply(this, parameters);
}