'use strict';

exports = module.exports = require('./dist/PhoneInputNativeDefaultMetadata').default;
exports.formatPhoneNumber = require('./dist/formatPhoneNumberDefaultMetadata').default;
exports.isValidPhoneNumber = require('./dist/isValidPhoneNumberDefaultMetadata').default;
exports['default'] = require('./dist/PhoneInputNativeDefaultMetadata').default;
