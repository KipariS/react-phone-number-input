/**
 * @module react-phone-number-input
 */
declare module 'react-phone-number-input' {

    import * as React from "react";

    /**
     * @interface Props
     */
    interface Props {
        name?: string,
        placeholder?: string;
        value?: string;
        onChange?: (value: string) => void;
        inputClassName?: string;
        labels?: {
            [iso: string]: string
        };
        limitMaxLength?: boolean;
        displayInitialValueAsLocalNumber?: boolean;
        countryOptions?: string[];
        country?: string;
    }

    /**
     * @class PhoneInput
     */
    export default class PhoneInput extends React.Component<Props, any> {

    }
}
