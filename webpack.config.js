const path = require('path');

module.exports = {
    entry: {
        PhoneInputNativeDefaultMetadata: './src/PhoneInputNativeDefaultMetadata',
        formatPhoneNumberDefaultMetadata: './src/formatPhoneNumberDefaultMetadata',
        isValidPhoneNumberDefaultMetadata: './src/isValidPhoneNumberDefaultMetadata',
    },
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: `[name].js`,
        library: ['name']
    },
    devtool: false,
    // devtool: "inline-cheap-source-map",
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                include: [path.resolve(__dirname, "src")],
                exclude: /node_modules/,
                loaders: 'babel-loader'
            },
        ]
    },
};
